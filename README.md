# Arangodb Alpine

The goal here is building an Alpine linux package for Arangodb.

### Jump into the build environment:

```sh
docker run -it -v `pwd`:/home/mike mikewilliamson/arango-alpine
```
